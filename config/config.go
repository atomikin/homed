package config

import (
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

/*
DBConf is a database related part of the config
*/
type DBConf struct {
	URI      string `yaml:"uri"`
	DebugLog bool   `yaml:"debug_log"`
	Dialect  string `yaml:"dialect"`
}

/*
APIConf - API config of the app
*/
type APIConf struct {
	Host               string `yaml:"host"`
	Port               string `yaml:"port"`
	MaxChunkSize       uint   `yaml:"max_chunk_size"`
	MaxFileSize        uint64 `yaml:"max_file_size"`
	TokenLifetimeHours int64  `yaml:"token_lifetime_hours"`
}

/*
Configuraton is an object representation of the application config JSON
*/
type Configuraton struct {
	API APIConf `yaml:"api"`
	DB  DBConf  `yaml:"db"`
}

/**
applyDefaults - Apply some default values to the Configuration
*/
func applyDefaults(conf *Configuraton) {
	if conf.API.TokenLifetimeHours == 0 {
		conf.API.TokenLifetimeHours = 24
	}
}

/*
Load configuration file
*/
func Load(path string) *Configuraton {
	file, err := os.Open(path)
	if err != nil {
		log.Fatalf("Config file %s does not exist: %s", path, err.Error())
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("Error reading config: %s", err.Error())
	}
	con := new(Configuraton)
	err = yaml.Unmarshal(data, con)
	if err != nil {
		log.Fatalf("Incorrect config: %s", err.Error())
	}
	if err != nil {
		log.Fatal(err.Error())
	}
	applyDefaults(con)
	return con
}
