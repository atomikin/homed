package main

import (
	"log"
	"os"

	_ "github.com/jackc/pgx/v4"
	"gitlab.com/atomikin/home-db/config"
	"gitlab.com/atomikin/home-db/daemon"
)

func main() {
	config := config.Load(os.Args[1])
	if os.Args[2] == "daemon" {
		daemon.Run(config)
	} else {
		log.Fatal("Illegal operation mode")
	}
}
