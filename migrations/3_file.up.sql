alter table files drop column blob;
alter table files add column blob bytea default null;
alter table files add column sha256 text default null;
alter table files add constraint unique_item_file unique(name, item_id);