alter table files drop column blob;
alter table files add column blob text;
alter table files drop column sha256;
alter table files drop constraint unique_item_file;