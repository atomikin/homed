create table base (
    created_at TIMESTAMP DEFAULT current_timestamp,
    updated_at TIMESTAMP DEFAULT current_timestamp
);

create table containers(
    id serial primary key,
    name text unique not null,
    description text not null,
    location text not null
) inherits (base);

create type item_type as enum ('document', 'object', 'medicine');
create type file_type as enum ('photo', 'scan');

create table items (
    id serial primary key,
    i_type item_type not null,
    name text unique not null,
    quantity bigint DEFAULT 1,
    container_id BIGINT references containers(id)
) inherits (base);

create table files(
    id serial primary key,
    item_id BIGINT references items(id) on delete cascade,
    blob text,
    name text not null,
    f_type file_type
) inherits (base);

create table tags (
    id serial primary key,
    name TEXT
);

create table item_tags (
    tag_id BIGINT REFERENCES tags(id) ON DELETE Cascade,
    item_id BIGINT REFERENCES items(id) ON DELETE Cascade
);

create or replace view items_with_tags as
    select
    *,
    array(
        select 
            name 
        from tags t join item_tags it
        on t.id = it.tag_id
        where item_id = i.id) as tags
    from items i;

create or replace function insert_item(itype item_type, this_name text, container_id bigint, this_tags text[], this_quantity bigint)
returns bigint as $$
declare new_id bigint;
begin
    insert into items
        (i_type, name, container_id, quantity) values 
        (itype, this_name, container_id, this_quantity)
        returning id into new_id;

    with inserted_tags as (
       insert into tags (name)
        select n as name from unnest(this_tags) as n 
        on conflict do nothing
        returning id
    )

    insert into item_tags
        select id, new_id as item_id from inserted_tags
    on conflict do nothing;
    return new_id;
end;
$$ LANGUAGE plpgsql;

create or replace function update_item(this_id bigint, itype item_type, this_name text, this_container_id bigint, this_tags text[], this_quantity bigint)
returns bigint as $$
declare new_id bigint;
begin
    update items
        set 
            i_type = itype, 
            name = this_name, 
            container_id = this_container_id,
            updated_at = current_timestamp,
            quantity = this_quantity

        where id = this_id
        returning id into new_id;

    delete from item_tags 
    where item_id = new_id;

    with inserted_tags as (
       insert into tags (name)
        select n as name from unnest(this_tags) as n 
        on conflict do nothing
        returning id
    )

    insert into item_tags
        select id, new_id as item_id from inserted_tags
    on conflict do nothing;
    return new_id;
end;
$$ LANGUAGE plpgsql;

create index files_name_index on files(name);
create index items_name_index on items(name);
create index containers_name_index on containers(name);
create index containers_location_index on containers(location);
create index cont_text_ind on containers using GIN(
    to_tsvector('english', name || ' ' || location || ' ' || description)
);
create index item_text_ind on items using GIN(
    to_tsvector('english', name)
);

