drop type item_type;
drop index files_name_index;
drop index items_name_index;
drop index containers_name_index;
drop index containers_location_index;

drop function update_item;
drop function insert_item;
drop view items_with_tags;

drop table item_tags;
drop table tags;
drop table files;
drop table items;
drop table containers;
