create table users (
    id serial primary key,
    name text unique NOT NULL,
    digest text not null
);

create type user_role as enum ('read', 'write');

create table user_access_rights (
    id serial primary key,
    user_id BIGINT references users(id),
    role user_role,
    resource text,
    unique(role, user_id, resource)
);

insert into users (name, digest) values ('jack', '2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae');
