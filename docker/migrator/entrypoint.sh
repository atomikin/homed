#!/bin/bash
set -exu
exec migrate --source file://app/migrations --database \
    'postgresql://homedb:der_parol@postgres:5432/homedb?sslmode=disable' up 3