package resps

import (
	"errors"
	"log"

	"gitlab.com/atomikin/home-db/model"
)

// Representation -
type Representation interface {
	FromModel(interface{}) error
	ToModel(interface{}) error
}

// Response -
type Response struct {
	Version      int         `json:"version"`
	Entity       string      `json:"entity"`
	Status       string      `json:"status"`
	Data         interface{} `json:"data"`
	ResponseCode int         `json:"response_code"`
}

// Collection -
type Collection struct {
	Version      int         `json:"version"`
	Entity       string      `json:"entity"`
	Status       string      `json:"status"`
	Count        int         `json:"count"`
	Data         interface{} `json:"data"`
	ResponseCode int         `json:"response_code"`
}

// ToModel -
func (resp *Response) ToModel(md interface{}) error {
	if resp.Entity == "container" {
		parsedModel, ok := md.(model.Container)
		if !ok {
			return errors.New("Illegal model type")
		}
		incomingModel, ok := resp.Data.(model.Container)
		if !ok {
			return errors.New("Illegal model type")
		}
		parsedModel.Location = incomingModel.Location
		parsedModel.Name = incomingModel.Name
		parsedModel.Description = incomingModel.Description

	} else if resp.Entity == "item" {
		parsedModel, ok := md.(model.Item)
		if !ok {
			return errors.New("Illegal model type")
		}
		incomingModel, ok := resp.Data.(model.Item)
		if !ok {
			return errors.New("Illegal model type")
		}
		parsedModel.ContainerID = incomingModel.ContainerID
		parsedModel.Name = incomingModel.Name
		parsedModel.Tags = incomingModel.Tags
		parsedModel.ItemType = incomingModel.ItemType
	}
	return nil
}

//FromModel -
func (resp *Response) FromModel(md interface{}) error {
	resp.Version = 1
	resp.Status = "ok"
	if _, ok := md.(*model.Container); ok {
		resp.Entity = "container"
		resp.Data = md
		return nil
	}
	if _, ok := md.(*model.Item); ok {
		resp.Entity = "item"
		resp.Data = md
		return nil
	}
	if _, ok := md.(*model.File); ok {
		resp.Entity = "file"
		resp.Data = md
		return nil
	}
	return errors.New("internal error")
}

// ToModel -
func (resp *Collection) ToModel(md interface{}) error {
	if resp.Entity == "container" {
		incomingModel, ok := resp.Data.(*model.Containers)
		if !ok {
			return errors.New("Illegal model type")
		}
		resModel, ok := md.(*model.Containers)
		if !ok {
			return errors.New("Illegal model type")
		}
		*resModel = *incomingModel
	} else if resp.Entity == "item" {
		incomingModel, ok := resp.Data.(*model.Items)
		if !ok {
			return errors.New("Illegal model type")
		}
		resp.Data = incomingModel
	}
	return nil
}

//FromModel -
func (resp *Collection) FromModel(md interface{}) error {
	resp.Status = "ok"
	resp.Version = 1
	resp.ResponseCode = 200
	if mdd, ok := md.(*model.Containers); ok {
		log.Print(mdd)
		resp.Entity = "containers"
		resp.Data = mdd
		resp.Count = len(*mdd)
		return nil
	} else if res, ok := md.(*model.Items); ok {
		log.Print(res)
		resp.Entity = "items"
		resp.Data = res
		resp.Count = len(*res)
		return nil
	} else if res, ok := md.(*model.Files); ok {
		// log.Print(res)
		resp.Entity = "files"
		resp.Data = res
		resp.Count = len(*res)
		return nil
	}
	return errors.New("internal error")
}
