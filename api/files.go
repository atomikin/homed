package api

import (
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/atomikin/home-db/model"
	"gitlab.com/atomikin/home-db/provider"
	"gitlab.com/atomikin/home-db/repository"
	"gitlab.com/atomikin/home-db/resps"
)

const boundary = "gdfgskhfkjshafyfhksvFDSFfgsg"

func prepareFileError(err error, status int, w http.ResponseWriter) resps.Representation {
	resp := new(resps.Response)
	resp.Status = "error"
	resp.Version = 1
	resp.ResponseCode = status
	resp.Entity = "file"
	resp.Data = err.Error()
	w.WriteHeader(status)
	return resp
}

func getFiles(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("file", tx)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	data := make(model.Files, 0)
	params := make([]repository.Arg, 0)
	query := r.URL.Query()
	for _, i := range []string{"name", "item_id"} {
		param := query.Get(i)
		if param != "" {
			params = append(params, repository.Arg{i, param, "="})
		}
	}
	err = repo.FindBy(&data, params...)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	res := resps.Collection{}
	if err = (&res).FromModel(&data); err != nil {
		resp := resps.Response{Status: err.Error(), Version: 1}
		w.WriteHeader(http.StatusOK)
		return &resp
	}
	return &res
}

// Seems like it can be generalized, though refactoring should be done later
func getFile(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("file", tx)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	data := model.File{}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		return prepareFileError(errors.New("ID is required"), http.StatusBadRequest, w)
	}
	err = repo.Find(id, &data)
	if data.ID == 0 {
		return prepareFileError(
			fmt.Errorf("File with ID %s not found", id),
			http.StatusBadRequest, w,
		)
	}
	if err != nil {
		log.Print("Error: " + err.Error())
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	res := resps.Response{}
	log.Print("Data: ")
	log.Print(data)
	if err = (&res).FromModel(&data); err != nil {
		resp := resps.Response{
			Status: "error", Data: err.Error(), Version: 1,
			ResponseCode: 500,
		}
		w.WriteHeader(http.StatusOK)
		return &resp
	}
	res.ResponseCode = 200
	return &res
}

// CreateFile -
func createFile(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	log.Print("Entered file creation")
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("file", tx)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	file := new(model.File)
	log.Print("1")
	err = r.ParseMultipartForm(int64(pr.Config().API.MaxFileSize)) // maxMemory 8MB
	log.Print("2")
	log.Print(r.MultipartForm)
	if err != nil {
		return prepareFileError(
			err,
			http.StatusRequestEntityTooLarge,
			w,
		)
	}
	// log.Print("3")
	data := r.FormValue("metadata")
	// log.Print("4")
	log.Printf("data %s", string(data))
	err = json.Unmarshal([]byte(data), file)
	if err != nil {
		return prepareFileError(err, http.StatusBadRequest, w)
	}
	// for _, h := range r.MultipartForm.File["media"] {
	f, _, err := r.FormFile("media")
	if err != nil {
		return prepareFileError(err, http.StatusBadRequest, w)
	}
	file.Blob, err = ioutil.ReadAll(f)
	if err != nil {
		return prepareFileError(err, http.StatusBadRequest, w)
	}
	sum := strings.ToLower(fmt.Sprintf("%x", sha256.Sum256(file.Blob)))
	if sum != strings.ToLower(file.SHA256) {
		return prepareFileError(
			errors.New("Blob hash mismatch: "+sum+" (calculated) and "+file.SHA256),
			http.StatusBadRequest,
			w,
		)
	}
	file.SHA256 = sum
	// break
	// }
	i, _ := json.Marshal(file)
	log.Print(string(i))
	err = repo.Store(file)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	resp := new(resps.Response)
	resp.Data = file
	resp.ResponseCode = 201
	resp.Version = 1
	resp.Entity = "file"
	resp.Status = "ok"
	w.WriteHeader(http.StatusCreated)
	return resp
}

func deleteFile(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("file", tx)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		return prepareFileError(errors.New("ID is required"), http.StatusNotFound, w)
	}
	log.Printf("Got DELETE request for ID %s", id)
	file := new(model.File)
	err = repo.Find(id, file)
	if err != nil {
		return prepareFileError(err, http.StatusNotFound, w)
	}
	err = repo.Delete(file)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	w.WriteHeader(http.StatusOK)
	return &resps.Response{
		Entity:       "file",
		Status:       "ok",
		Data:         file,
		ResponseCode: 200,
		Version:      1,
	}
}

func updateFile(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("file", tx)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		return prepareFileError(errors.New("ID is required"), http.StatusNotFound, w)
	}
	file := new(model.File)
	err = repo.Find(id, file)
	if err != nil {
		return prepareFileError(err, http.StatusNotFound, w)
	}
	err = r.ParseMultipartForm(int64(pr.Config().API.MaxFileSize)) // maxMemory 8MB
	if err != nil {
		return prepareFileError(
			err,
			http.StatusRequestEntityTooLarge,
			w,
		)
	}
	data := r.FormValue("metadata")
	oldSHA := file.SHA256
	err = json.Unmarshal([]byte(data), file)
	if err != nil {
		return prepareFileError(err, http.StatusBadRequest, w)
	}
	if strings.ToLower(file.SHA256) != oldSHA {

		f, _, err := r.FormFile("media")
		if err != nil {
			return prepareFileError(err, http.StatusBadRequest, w)
		}
		file.Blob, err = ioutil.ReadAll(f)
		if err != nil {
			return prepareFileError(err, http.StatusBadRequest, w)
		}
		sum := strings.ToLower(fmt.Sprintf("%x", sha256.Sum256(file.Blob)))
		if sum != strings.ToLower(file.SHA256) {
			return prepareFileError(
				errors.New("Blob hash mismatch: "+sum+" (calculated) and "+file.SHA256),
				http.StatusBadRequest,
				w,
			)
		}
		file.SHA256 = sum
	}

	err = repo.Update(file)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	return &resps.Response{
		Version:      1,
		Data:         file,
		ResponseCode: 200,
		Entity:       "file",
		Status:       "ok",
	}
}

func getFileBlob(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("file", tx)
	if err != nil {
		return prepareFileError(err, http.StatusInternalServerError, w)
	}
	file := model.File{}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		resp := resps.Response{
			Version: 1, Status: "error", Data: "ID is required",
			ResponseCode: 404,
		}
		w.WriteHeader(http.StatusNotFound)
		return &resp
	}
	err = repo.Find(id, &file)
	if file.ID == 0 {
		resp := resps.Response{
			Version: 1, Status: "error",
			Data:         errors.New("Not Found"),
			ResponseCode: 404,
		}
		w.WriteHeader(http.StatusNotFound)
		return &resp
	}
	w.Header().Set("Content-Disposition", "attachment; filename="+file.Name)
	w.Write(file.Blob)
	return &resps.Response{}
}
