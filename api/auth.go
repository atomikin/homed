package api

import (
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"gitlab.com/atomikin/home-db/repository"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/atomikin/home-db/model"
	"gitlab.com/atomikin/home-db/provider"
	"gitlab.com/atomikin/home-db/resps"
)

func prepareAuthError(err error, status int, w http.ResponseWriter) resps.Representation {
	resp := new(resps.Response)
	resp.Status = "error"
	resp.Version = 1
	resp.ResponseCode = status
	resp.Entity = "authorization"
	resp.Data = err.Error()
	w.WriteHeader(status)
	return resp
}

func getToken(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	if err != nil {
		return prepareAuthError(err, http.StatusInternalServerError, w)
	}
	defer tx.Commit()
	user := new(model.User)
	repo, err := pr.Repo("user", tx)
	if err != nil {
		return prepareAuthError(err, http.StatusInternalServerError, w)
	}
	content, err := ioutil.ReadAll(r.Body)

	if err != nil {
		return prepareAuthError(err, http.StatusInternalServerError, w)
	}
	err = json.Unmarshal(content, user)
	if err != nil {
		return prepareAuthError(err, http.StatusUnauthorized, w)
	}
	users := make(model.Users, 0)
	err = repo.FindBy(
		&users,
		repository.Arg{
			Field: "name",
			Value: user.Name,
		},
		repository.Arg{
			Field: "digest",
			Value: strings.ToLower(
				fmt.Sprintf("%x", sha256.Sum256([]byte(user.Digest))),
			),
		},
	)
	if err != nil {
		log.Printf("ERROR: %s", err.Error())
		return prepareAuthError(err, http.StatusUnauthorized, w)
	}
	if len(users) != 1 {
		return prepareAuthError(errors.New("User not found"), http.StatusUnauthorized, w)
	}
	user = &users[0]
	token := jwt.NewWithClaims(
		jwt.SigningMethodHS256,
		jwt.MapClaims{
			"admin":   true,
			"name":    user.Name,
			"user_id": user.ID,
			"exp":     time.Now().Add(time.Hour * time.Duration(pr.Config().API.TokenLifetimeHours)).Unix(),
		},
	)
	tokenString, err := token.SignedString(secret)
	if err != nil {
		log.Printf("ERROR: %s", err.Error())
		return prepareAuthError(err, http.StatusInternalServerError, w)
	}
	return &resps.Response{
		Version:      1,
		Status:       "ok",
		ResponseCode: 201,
		Data:         tokenString,
		Entity:       "jwt",
	}
}
