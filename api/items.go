package api

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/atomikin/home-db/model"
	"gitlab.com/atomikin/home-db/provider"
	"gitlab.com/atomikin/home-db/repository"
	"gitlab.com/atomikin/home-db/resps"
)

func prepareItemError(err error, status int, w http.ResponseWriter) resps.Representation {
	resp := new(resps.Response)
	resp.Status = "error"
	resp.Version = 1
	resp.ResponseCode = status
	resp.Entity = "item"
	resp.Data = err.Error()
	w.WriteHeader(status)
	return resp
}

func getItems(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("item", tx)
	if err != nil {
		return prepareItemError(err, http.StatusInternalServerError, w)
	}
	data := make(model.Items, 0)
	params := make([]repository.Arg, 0)
	query := r.URL.Query()
	for _, i := range []string{"name", "i_type", "container_id", "tags", "ftext"} {
		param := query.Get(i)
		if param != "" {
			params = append(params, repository.Arg{i, param, "="})
		}
	}
	err = repo.FindBy(&data, params...)
	if err != nil {
		return prepareItemError(err, http.StatusInternalServerError, w)
	}
	res := resps.Collection{}
	if err = (&res).FromModel(&data); err != nil {
		resp := resps.Response{Status: err.Error(), Version: 1}
		w.WriteHeader(http.StatusOK)
		return &resp
	}
	return &res
}

func getItem(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("item", tx)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	data := model.Item{}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		resp := resps.Response{
			Version: 1, Status: "error", Data: "ID is required",
			ResponseCode: 404,
		}
		w.WriteHeader(http.StatusNotFound)
		return &resp
	}
	err = repo.Find(id, &data)
	if data.ID == 0 {
		resp := resps.Response{
			Version: 1, Status: "error",
			Data:         errors.New("Not Found"),
			ResponseCode: 404,
		}
		w.WriteHeader(http.StatusNotFound)
		return &resp
	}
	if err != nil {
		log.Print("Error: " + err.Error())
		return prepareError(err, http.StatusInternalServerError, w)
	}
	res := resps.Response{}
	log.Print("Data: ")
	log.Print(data)
	if err = (&res).FromModel(&data); err != nil {
		resp := resps.Response{
			Status: "error", Data: err.Error(), Version: 1,
			ResponseCode: 500,
		}
		w.WriteHeader(http.StatusOK)
		return &resp
	}
	res.ResponseCode = 200
	return &res
}

func updateItem(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("item", tx)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		return prepareError(errors.New("ID is required"), http.StatusNotFound, w)
	}
	item := new(model.Item)
	err = repo.Find(id, item)
	log.Print(item)
	if err != nil {
		return prepareError(err, http.StatusNotFound, w)
	}
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return prepareError(err, http.StatusBadRequest, w)
	}
	err = json.Unmarshal(data, item)
	if err != nil {
		return prepareError(err, http.StatusBadRequest, w)
	}
	log.Print(item)
	err = repo.Update(item)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	return &resps.Response{
		Version:      1,
		Data:         item,
		ResponseCode: 200,
		Entity:       "item",
		Status:       "ok",
	}
}

func deleteItem(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("item", tx)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		return prepareError(errors.New("ID is required"), http.StatusNotFound, w)
	}
	item := new(model.Item)
	err = repo.Find(id, item)
	if err != nil {
		return prepareError(err, http.StatusNotFound, w)
	}
	err = repo.Delete(item)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	w.WriteHeader(http.StatusOK)
	return &resps.Response{
		Entity:       "item",
		Status:       "ok",
		Data:         item,
		ResponseCode: 200,
		Version:      1,
	}
}

func createItem(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("item", tx)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	item := new(model.Item)
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return prepareError(err, http.StatusBadRequest, w)
	}
	err = json.Unmarshal(data, item)
	if err != nil {
		return prepareError(err, http.StatusBadRequest, w)
	}
	err = repo.Store(item)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	resp := new(resps.Response)
	resp.Data = item
	resp.ResponseCode = 201
	resp.Version = 1
	resp.Entity = "item"
	resp.Status = "ok"
	w.WriteHeader(http.StatusCreated)
	return resp
}
