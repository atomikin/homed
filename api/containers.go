package api

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"

	"gitlab.com/atomikin/home-db/model"
	"gitlab.com/atomikin/home-db/provider"
	"gitlab.com/atomikin/home-db/repository"
	"gitlab.com/atomikin/home-db/resps"
)

func prepareError(err error, status int, w http.ResponseWriter) resps.Representation {
	resp := new(resps.Response)
	resp.Status = "error"
	resp.Version = 1
	resp.ResponseCode = status
	resp.Entity = "container"
	resp.Data = err.Error()
	w.WriteHeader(status)
	return resp
}

func getContainers(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tk := context.Get(r, "user").(*model.Token)
	log.Printf("Logged in as user ID %d", tk.UserID)
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("container", tx)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	data := make(model.Containers, 0)
	// var data model.Containers
	params := make([]repository.Arg, 0)
	query := r.URL.Query()
	for _, i := range []string{"name", "location", "description", "ftext"} {
		param := query.Get(i)
		if param != "" {
			params = append(params, repository.Arg{i, param, "="})
		}
	}
	err = repo.FindBy(&data, params...)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	res := resps.Collection{}
	if err = (&res).FromModel(&data); err != nil {
		resp := resps.Response{Status: err.Error(), Version: 1}
		w.WriteHeader(http.StatusOK)
		return &resp
	}
	return &res
}

func getContainer(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("container", tx)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	data := model.Container{}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		resp := resps.Response{
			Version: 1, Status: "error", Data: "ID is required",
			ResponseCode: 404,
		}
		w.WriteHeader(http.StatusNotFound)
		return &resp
	}
	err = repo.Find(id, &data)
	if data.ID == 0 {
		resp := resps.Response{
			Version: 1, Status: "error",
			Data:         errors.New("Not Found"),
			ResponseCode: 404,
		}
		w.WriteHeader(http.StatusNotFound)
		return &resp
	}
	if err != nil {
		log.Print("Error: " + err.Error())
		return prepareError(err, http.StatusInternalServerError, w)
	}
	res := resps.Response{}
	if err = (&res).FromModel(&data); err != nil {
		resp := resps.Response{
			Status: "error", Data: err.Error(), Version: 1,
			ResponseCode: 500,
		}
		w.WriteHeader(http.StatusOK)
		return &resp
	}
	return &res
}

// CreateContainer -
func createContainer(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("container", tx)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	container := new(model.Container)
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return prepareError(err, http.StatusBadRequest, w)
	}
	err = json.Unmarshal(data, container)
	if err != nil {
		return prepareError(err, http.StatusBadRequest, w)
	}
	err = repo.Store(container)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	resp := new(resps.Response)
	resp.Data = container
	resp.ResponseCode = 201
	resp.Version = 1
	resp.Entity = "container"
	resp.Status = "ok"
	w.WriteHeader(http.StatusCreated)
	return resp
}

func deleteContainer(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("container", tx)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		return prepareError(errors.New("ID is required"), http.StatusNotFound, w)
	}
	log.Printf("Got DELETE request for ID %s", id)
	container := new(model.Container)
	err = repo.Find(id, container)
	if err != nil {
		return prepareError(err, http.StatusNotFound, w)
	}
	err = repo.Delete(container)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	w.WriteHeader(http.StatusOK)
	return &resps.Response{
		Entity:       "container",
		Status:       "ok",
		Data:         container,
		ResponseCode: 200,
		Version:      1,
	}
}

func updateContainer(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation {
	tx, err := pr.DB().Beginx()
	defer tx.Commit()
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	repo, err := pr.Repo("container", tx)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	params := mux.Vars(r)
	id := params["id"]
	if id == "" {
		return prepareError(errors.New("ID is required"), http.StatusNotFound, w)
	}
	container := new(model.Container)
	err = repo.Find(id, container)
	if err != nil {
		return prepareError(err, http.StatusNotFound, w)
	}
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return prepareError(err, http.StatusBadRequest, w)
	}
	err = json.Unmarshal(data, container)
	if err != nil {
		return prepareError(err, http.StatusBadRequest, w)
	}
	err = repo.Update(container)
	if err != nil {
		return prepareError(err, http.StatusInternalServerError, w)
	}
	return &resps.Response{
		Version:      1,
		Data:         container,
		ResponseCode: 200,
		Entity:       "container",
		Status:       "ok",
	}
}
