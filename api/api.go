package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"gitlab.com/atomikin/home-db/model"
	"gitlab.com/atomikin/home-db/resps"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"gitlab.com/atomikin/home-db/provider"
)

var secret = []byte("hmacSampleSecret")

// Handler -
type Handler func(pr provider.IProvider, w http.ResponseWriter, r *http.Request) resps.Representation

// ApiHandler -
type ApiHandler func(w http.ResponseWriter, r *http.Request)

// JwtVerify -
func JwtVerify(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var header = r.Header.Get("x-access-token")

		sendEncodedResponse := func(r resps.Response) {
			w.WriteHeader(http.StatusForbidden)
			json.NewEncoder(w).Encode(r)
		}

		resp := resps.Response{
			Version:      1,
			Status:       "error",
			ResponseCode: 401,
			Data:         "",
			Entity:       "authorization",
		}

		json.NewEncoder(w).Encode(r)
		header = strings.TrimSpace(header)
		if header == "" {
			resp.Data = "Token is missing"
			sendEncodedResponse(resp)
			return
		}

		tk := &model.Token{}
		token, err := jwt.ParseWithClaims(header, tk, func(token *jwt.Token) (interface{}, error) {
			return secret, nil
		})

		if err != nil {
			log.Printf("ERROR PARSING: %s", err.Error())
			resp.Data = err.Error()
			sendEncodedResponse(resp)
			return
		}
		if !token.Valid {
			log.Printf(`Access with expired token %s`, token.Raw)
			resp.Data = "Access with expired token \"" + token.Raw + "\""
			sendEncodedResponse(resp)
			return
		}
		log.Print(tk)
		context.Set(r, "user", tk)
		next.ServeHTTP(w, r)
	})
}

// Run API
func Run(pr provider.IProvider) {
	respond := func(handler Handler) ApiHandler {
		return func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("content-type", "application/json")
			result := handler(pr, w, r)
			if data, err := json.Marshal(result); err != nil {
				data, _ = json.Marshal(resps.Response{Version: 1, Status: "error", Data: err.Error()})
				// w.WriteHeader(http.StatusInternalServerError)
				w.Write(data)
			} else {
				// w.WriteHeader(http.StatusOK)
				w.Write(data)
			}
		}
	}
	respondBinary := func(handler Handler) ApiHandler {
		return func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("content-type", "application/octet-stream")
			handler(pr, w, r)
		}
	}
	r := mux.NewRouter()
	sec := r.PathPrefix("/auth").Subrouter()
	sec.Use(JwtVerify)
	sec.HandleFunc("/containers", respond(getContainers)).Methods("GET")
	sec.HandleFunc("/containers/{id}", respond(getContainer)).Methods("GET")
	sec.HandleFunc("/containers/{id}", respond(deleteContainer)).Methods("DELETE")
	sec.HandleFunc("/containers/{id}", respond(updateContainer)).Methods("PATCH")
	sec.HandleFunc("/containers", respond(createContainer)).Methods("POST")
	sec.HandleFunc("/items", respond(getItems)).Methods("GET")
	sec.HandleFunc("/items", respond(createItem)).Methods("POST")
	sec.HandleFunc("/items/{id}", respond(getItem)).Methods("GET")
	sec.HandleFunc("/items/{id}", respond(deleteItem)).Methods("DELETE")
	sec.HandleFunc("/items/{id}", respond(updateItem)).Methods("PATCH")
	sec.HandleFunc("/files", respond(getFiles)).Methods("GET")
	sec.HandleFunc("/files", respond(createFile)).Methods("POST")
	sec.HandleFunc("/files/{id}", respond(getFile)).Methods("GET")
	sec.HandleFunc("/files/{id}/blob", respondBinary(getFileBlob)).Methods("GET")
	sec.HandleFunc("/files/{id}", respond(deleteFile)).Methods("DELETE")
	sec.HandleFunc("/files/{id}", respond(updateFile)).Methods("PATCH")
	r.HandleFunc("/auth", respond(getToken)).Methods("POST")
	srv := &http.Server{
		Handler:      r,
		Addr:         fmt.Sprintf("%s:%s", pr.Config().API.Host, pr.Config().API.Port),
		WriteTimeout: 300 * time.Second,
		ReadTimeout:  300 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())

}
