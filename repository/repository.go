package repository

import (
	"errors"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/atomikin/home-db/model"
)

type Arg struct {
	Field    string
	Value    interface{}
	Operator string
}

func (argument *Arg) String(i int) string {
	operator := "="
	if argument.Operator != "" {
		operator = argument.Operator
	}
	return fmt.Sprintf("%s %s $%d", argument.Field, operator, i)
}

// Repository interface
type Repository interface {
	FindAll(interface{}) error
	Find(interface{}, interface{}) error
	FindBy(interface{}, ...Arg) error
	Delete(interface{}) error
	Store(interface{}) error
	Update(interface{}) error
}

// ContainerRepository -
type ContainerRepository struct {
	Tx *sqlx.Tx
}

// ItemRepository -
type ItemRepository struct {
	Tx *sqlx.Tx
}

// UserRepository -
type UserRepository struct {
	Tx *sqlx.Tx
}

// FileRepository -
type FileRepository struct {
	Tx *sqlx.Tx
}

// FindAll -
func (repo ContainerRepository) FindAll(result interface{}) error {
	rs, ok := result.(*model.Containers)
	if !ok {
		return errors.New("Illegal Argument")
	}
	err := repo.Tx.Select(rs, `select * from containers`)
	// log.Print(rs)
	return err
}

// Find -
func (repo ContainerRepository) Find(id interface{}, result interface{}) error {
	rs, ok := result.(*model.Container)
	if !ok {
		return errors.New("Illegal Argument")
	}
	return repo.Tx.Get(rs, `select * from containers where id = $1`, id)
}

// FindBy -
func (repo ContainerRepository) FindBy(result interface{}, args ...Arg) error {
	rs, ok := result.(*model.Containers)
	if !ok {
		return errors.New("Illegal Argument")
	}
	query := ""
	params := make([]interface{}, 0, len(args))
	for i, q := range args {
		if i != 0 {
			query += " AND "
		}
		log.Printf("Search field: %s", q.Field)
		if q.Field == "ftext" {
			query += fmt.Sprintf(
				`to_tsvector('english', name || ' ' || location || ' ' 
				|| description) @@ to_tsquery('english', $%d)`, i+1,
			)
		} else {
			query += q.String(i + 1)
		}
		if q.Value == nil {
			continue
		}
		params = append(params, q.Value)
	}
	resultQuery := `select * from containers`
	if query != "" {
		resultQuery += " where " + query
	}
	log.Print(resultQuery)
	log.Print(params)
	err := repo.Tx.Select(rs, resultQuery, params...)
	log.Print(rs)
	return err
}

// Delete -
func (repo ContainerRepository) Delete(obj interface{}) error {
	container, ok := obj.(*model.Container)
	if !ok {
		return errors.New("Illegal object type")
	}
	res, err := repo.Tx.Exec(`delete from containers where id = $1`, container.ID)
	if err != nil {
		log.Printf("Failed to delete container, error: %s", err.Error())
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf(`Deleted %d rows`, count)
	return nil
}

// Store -
func (repo ContainerRepository) Store(obj interface{}) error {
	container, ok := obj.(*model.Container)
	if !ok {
		return errors.New("Illegal object type")
	}
	err := repo.Tx.Get(container,
		`insert into containers (name, location, description) `+
			`values ($1, $2, $3) returning *`, container.Name, container.Location, container.Description,
	)
	if err != nil {
		return err
	}
	log.Printf(`Created container ID %d`, container.ID)
	return nil
}

// Update -
func (repo ContainerRepository) Update(obj interface{}) error {
	container, ok := obj.(*model.Container)
	if !ok {
		return errors.New("Illegal object type")
	}
	if container.ID == 0 {
		return errors.New("ID cannot be empty")
	}
	res, err := repo.Tx.NamedExec(
		`UPDATE containers set name = :name, location `+
			`= :location, description = :description where id = :id`,
		container,
	)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf(`Updated %d containers`, count)
	return nil
}

// type Repository interface {
// 	FindAll(interface{}) error
// 	Find(interface{}, interface{}) error
// 	FindBy(interface{}, ...Arg) error
// 	Delete(interface{}) error
// 	Store(interface{}) error
// 	Update(interface{}) error
// }
// FindAll -
func (repo ItemRepository) FindAll(result interface{}) error {
	rs, ok := result.(*model.Items)
	if !ok {
		return errors.New("Illegal Argument")
	}

	err := repo.Tx.Select(rs, `select * from items_with_tags`)
	log.Print(rs)
	return err
}

// Find -
func (repo ItemRepository) Find(id interface{}, result interface{}) error {
	rs, ok := result.(*model.Item)
	if !ok {
		return errors.New("Illegal Argument")
	}
	return repo.Tx.Get(rs, `select * from items_with_tags where id = $1`, id)
}

// FindBy -
func (repo ItemRepository) FindBy(result interface{}, args ...Arg) error {
	rs, ok := result.(*model.Items)
	if !ok {
		return errors.New("Illegal Argument")
	}
	query := ""
	params := make([]interface{}, 0, len(args))
	for i, q := range args {
		if q.Value == nil {
			continue
		}
		if i != 0 {
			query += " AND "
		}
		if q.Field == "tags" {
			query += fmt.Sprintf(` $%d = ANY(tags)`, i+1)
		} else if q.Field == "ftext" {
			query += fmt.Sprintf(
				`to_tsvector('english', name || ' ' || i_type::text) @@ to_tsquery('english', $%d)`, i+1,
			)
		} else {
			query += q.String(i + 1)
		}
		params = append(params, q.Value)
	}
	resultQuery := `select * from items_with_tags`
	if query != "" {
		resultQuery += " where " + query
	}
	log.Print(resultQuery)
	log.Print(params)
	err := repo.Tx.Select(rs, resultQuery, params...)
	log.Print(rs)
	return err
}

// Delete -
func (repo ItemRepository) Delete(obj interface{}) error {
	container, ok := obj.(*model.Item)
	if !ok {
		return errors.New("Illegal object type")
	}
	res, err := repo.Tx.Exec(`delete from items where id = $1`, container.ID)
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf(`Deleted %d rows`, count)
	return nil
}

// Store -
func (repo ItemRepository) Store(obj interface{}) error {
	item, ok := obj.(*model.Item)
	if !ok {
		return errors.New("Illegal object type")
	}
	var id int
	err := repo.Tx.QueryRow(
		`select insert_item($1, $2, $3, $4, $5) as id`,
		item.ItemType, item.Name, item.ContainerID, item.Tags, item.Quantity,
	).Scan(&id)
	if err != nil {
		return err
	}
	log.Printf(`Inserted item ID %d`, id)
	err = repo.Find(id, item)
	if err != nil {
		log.Printf(`Faced error %s`, err.Error())
		return err
	}
	log.Printf(`Found item ID %d`, item.ID)
	return nil
}

// Update -
func (repo ItemRepository) Update(obj interface{}) error {
	item, ok := obj.(*model.Item)
	if !ok {
		return errors.New("Illegal object type")
	}
	if item.ID == 0 {
		return errors.New("ID cannot be empty")
	}
	res, err := repo.Tx.NamedExec(
		`select * from update_item(:id, :i_type, :name, :container_id, :tags, :quantity)`, item,
	)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf(`Updated %d containers`, count)
	return nil
}

// ####################
// User repository
func (repo UserRepository) FindAll(result interface{}) error {
	rs, ok := result.(*model.Users)
	if !ok {
		return errors.New("Illegal Argument")
	}

	err := repo.Tx.Select(rs, `select * from users`)
	log.Print(rs)
	return err
}

// Find -
func (repo UserRepository) Find(id interface{}, result interface{}) error {
	rs, ok := result.(*model.User)
	if !ok {
		return errors.New("Illegal Argument")
	}
	return repo.Tx.Get(rs, `select * from users where id = $1`, id)
}

// FindBy -
func (repo UserRepository) FindBy(result interface{}, args ...Arg) error {
	rs, ok := result.(*model.Users)
	if !ok {
		return errors.New("Illegal Argument")
	}
	query := ""
	params := make([]interface{}, 0, len(args))
	for i, q := range args {
		if q.Value == nil {
			continue
		}
		if i != 0 {
			query += " AND "
		}
		query += q.String(i + 1)

		params = append(params, q.Value)
	}
	resultQuery := `select * from users`
	if query != "" {
		resultQuery += " where " + query
	}
	log.Print(resultQuery)
	log.Print(params)
	err := repo.Tx.Select(rs, resultQuery, params...)
	log.Print(rs)
	return err
}

// Delete -
func (repo UserRepository) Delete(obj interface{}) error {
	container, ok := obj.(*model.Item)
	if !ok {
		return errors.New("Illegal object type")
	}
	res, err := repo.Tx.Exec(`delete from users where id = $1`, container.ID)
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf(`Deleted %d rows`, count)
	return nil
}

// Store -
func (repo UserRepository) Store(obj interface{}) error {
	user, ok := obj.(*model.User)
	if !ok {
		return errors.New("Illegal object type")
	}
	var id int
	err := repo.Tx.QueryRow(
		`insert into users (name, diget) values ($1, $2) returning id`,
		user.Name, user.Digest,
	).Scan(&id)
	if err != nil {
		return err
	}
	log.Printf(`Inserted item ID %d`, id)
	err = repo.Find(id, user)
	if err != nil {
		log.Printf(`Faced error %s`, err.Error())
		return err
	}
	log.Printf(`Found item ID %d`, user.ID)
	return nil
}

// Update -
func (repo UserRepository) Update(obj interface{}) error {
	user, ok := obj.(*model.User)
	if !ok {
		return errors.New("Illegal object type")
	}
	if user.ID == 0 {
		return errors.New("ID cannot be empty")
	}
	res, err := repo.Tx.NamedExec(
		`update users set digest = :digest`, user,
	)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf(`Updated %d users`, count)
	return nil
}

// type Repository interface {
// 	FindAll(interface{}) error
// 	Find(interface{}, interface{}) error
// 	FindBy(interface{}, ...Arg) error
// 	Delete(interface{}) error
// 	Store(interface{}) error
// 	Update(interface{}) error
// }

// FindAll -
func (repo FileRepository) FindAll(result interface{}) error {
	rs, ok := result.(*model.Files)
	if !ok {
		return errors.New("Illegal Argument")
	}

	err := repo.Tx.Select(rs, `select * from files`)
	log.Print(rs)
	return err
}

// Find -
func (repo FileRepository) Find(id interface{}, result interface{}) error {
	rs, ok := result.(*model.File)
	if !ok {
		return errors.New("Illegal Argument")
	}
	return repo.Tx.Get(rs, `select * from files where id = $1`, id)
}

// FindBy -
func (repo FileRepository) FindBy(result interface{}, args ...Arg) error {
	rs, ok := result.(*model.Files)
	if !ok {
		return errors.New("Illegal Argument")
	}
	query := ""
	params := make([]interface{}, 0, len(args))
	for i, q := range args {
		if q.Value == nil {
			continue
		}
		if i != 0 {
			query += " AND "
		}
		query += q.String(i + 1)
		params = append(params, q.Value)
	}
	resultQuery := `select * from files`
	if query != "" {
		resultQuery += " where " + query
	}
	log.Print(resultQuery)
	log.Print(params)
	err := repo.Tx.Select(rs, resultQuery, params...)
	// log.Print(rs)
	return err
}

// Delete -
func (repo FileRepository) Delete(obj interface{}) error {
	file, ok := obj.(*model.File)
	if !ok {
		return errors.New("Illegal object type")
	}
	res, err := repo.Tx.Exec(`delete from files where id = $1`, file.ID)
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf(`Deleted %d rows`, count)
	return nil
}

// Store -
func (repo FileRepository) Store(obj interface{}) error {
	file, ok := obj.(*model.File)
	if !ok {
		return errors.New("Illegal object type")
	}
	err := repo.Tx.Get(
		file,
		`insert into files (name, item_id, blob, f_type, sha256) values ($1, $2, $3, $4, $5) returning *`,
		file.Name, file.ItemID, file.Blob, file.FileType, file.SHA256,
	)
	if err != nil {
		return err
	}
	log.Printf(`Inserted item ID %d`, file.ID)
	return nil
}

// Update -
func (repo FileRepository) Update(obj interface{}) error {
	file, ok := obj.(*model.File)
	if !ok {
		return errors.New("Illegal object type")
	}
	if file.ID == 0 {
		return errors.New("ID cannot be empty")
	}
	res, err := repo.Tx.NamedExec(
		`UPDATE files set name = :name, item_id `+
			`= :item_id, blob = :blob, f_type = :f_type, sha256 = :sha256 where id = :id`,
		file,
	)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf(`Updated %d containers`, count)
	return nil
}
