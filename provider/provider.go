package provider

import (
	"errors"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/atomikin/home-db/config"
	"gitlab.com/atomikin/home-db/repository"
)

// IProvider -
type IProvider interface {
	DB() *sqlx.DB
	Config() *config.Configuraton
	Repo(data string, tx *sqlx.Tx) (repository.Repository, error)
}

// Provider -
type Provider struct {
	db     *sqlx.DB
	config *config.Configuraton
}

// Config -
func (pr *Provider) Config() *config.Configuraton {
	return pr.config
}

// Repo -
func (pr *Provider) Repo(name string, tx *sqlx.Tx) (repository.Repository, error) {
	if name == "container" {
		return repository.ContainerRepository{Tx: tx}, nil
	} else if name == "item" {
		return repository.ItemRepository{Tx: tx}, nil
	} else if name == "user" {
		return repository.UserRepository{Tx: tx}, nil
	} else if name == "file" {
		return repository.FileRepository{Tx: tx}, nil
	} else {
		return nil, errors.New("Illegal repo name")
	}
}

// DB -
func (pr *Provider) DB() *sqlx.DB {
	err := pr.db.Ping()
	if err != nil {
		db, err := openDBConn(&pr.config.DB)
		if err != nil {
			log.Fatalf("fatal error: %s", err.Error())
		}
		pr.db = db
	}
	return pr.db
}

func openDBConn(cfg *config.DBConf) (*sqlx.DB, error) {
	return sqlx.Connect(cfg.Dialect, cfg.URI)
}

// New - new resource provider
func New(config *config.Configuraton) IProvider {
	db, err := sqlx.Connect(config.DB.Dialect, config.DB.URI)
	if err != nil {
		log.Fatal(err.Error())
	}
	res := new(Provider)
	res.db = db
	res.config = config
	return res
}
