package daemon

import (
	"fmt"
	"log"
	"os"
	"os/signal"

	"gitlab.com/atomikin/home-db/api"
	"gitlab.com/atomikin/home-db/config"
	"gitlab.com/atomikin/home-db/provider"
)

/*
Run start the sever daemon
*/
func Run(cfg *config.Configuraton) {
	//utils.InitLogger(cfg)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			fmt.Fprintln(os.Stderr, "Got SigInt, gracefully exiting...")
			os.Exit(1)
		}
	}()
	ct := provider.New(cfg)
	log.Printf(
		"Started media file server on http://%s:%s ...",
		cfg.API.Host, cfg.API.Port,
	)
	api.Run(ct)
}
