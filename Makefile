# PACKAGES = common/utils common/config api/controller api/model api daemon
IMPORT_PATH = gitlab.com/atomikin/home-db

# versioning
export RELEASE ?= minor
export VERSION ?= $(shell build-helper build-version --increment -r "${RELEASE}")
export BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)
export RESULT_NAME ?= homedb

RUN_CFG ?= ./test_conf.yml

export BUILD_DIR = ./build

all: save-version

app: libs
	go build  -o $(BUILD_DIR)/$(RESULT_NAME) $(IMPORT_PATH)

run: libs
	go run $(IMPORT_PATH) $(RUN_CFG)

libs: daemon api

build-dir:
	rm -rf ./build && mkdir ./build

config:
		go install $(IMPORT_PATH)/config

api: config model resps repository provider
		go install $(IMPORT_PATH)/api

# api-representation:
# 		go install $(IMPORT_PATH)/api/representation

model: config
		go install $(IMPORT_PATH)/model

# api-converter: api-model common-config
# 	go install $(IMPORT_PATH)/api/converter

# storage: common-config
# 		go install $(IMPORT_PATH)/storage

daemon: api provider
		go install $(IMPORT_PATH)/daemon

provider: config
	go install $(IMPORT_PATH)/provider

repository: config
	go install $(IMPORT_PATH)/config

# test: test-storage

# test-storage: storage
# 	go test $(IMPORT_PATH)/storage -run ''

container: app
	#docker build . -t "192.168.1.58:5000/internal/home-db:${VERSION}"
	#docker tag "192.168.1.58:5000/internal/home-db:${VERSION}" "192.168.1.58:5000/internal/home-db:latest"
	#docker push "192.168.1.58:5000/internal/home-db:${VERSION}"
	#docker push 192.168.1.58:5000/internal/home-db:latest
	python3 docker/pylib/build.py internal/home-db \
		--args "APP_NAME=${RESULT_NAME}" \
		--tags "${VERSION}" \
		--include-subpath files \
		--include "${BUILD_DIR}/${RESULT_NAME}" migrations/ \
		--docker-file docker/homedb/Dockerfile

migrator-container: container
	python3 docker/pylib/build.py internal/home-db/migrator \
		--tags "${VERSION}" \
		--docker-file docker/migrator/Dockerfile

save-version: container migrator-container 
	build-helper build-version --save "${VERSION}"
	git add version.ini
	git commit -m "Compiled ${VERSION}. Release: ${RELEASE}"
	git push origin "${BRANCH}"