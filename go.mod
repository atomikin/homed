module gitlab.com/atomikin/home-db

go 1.15

require (
	github.com/auth0/go-jwt-middleware v1.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx v3.6.2+incompatible // indirect
	github.com/jackc/pgx/v4 v4.9.2
	github.com/jinzhu/gorm v1.9.16
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.3.0
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.0.0-20201112155050-0c6587e931a9 // indirect
	golang.org/x/text v0.3.4 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
