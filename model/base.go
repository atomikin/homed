package model

import (
	"time"

	"github.com/lib/pq"
)

// Base - common fields
type Base struct {
	ID        uint64    `db:"id" json:"id"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

// Container - storage container
type Container struct {
	Base
	Name        string `db:"name" json:"name"`
	Description string `db:"description" json:"description"`
	Location    string `db:"location" json:"location"`
}

// Item -
type Item struct {
	Base
	ItemType    string         `db:"i_type" json:"i_type"`
	Name        string         `db:"name" json:"name"`
	Quantity    int64          `db:"quantity" json:"quantity"`
	Tags        pq.StringArray `db:"tags" json:"tags"`
	ContainerID uint64         `db:"container_id" json:"container_id"`
}

// File related to an item
type File struct {
	Base
	ItemID   uint64 `db:"item_id" json:"item_id"`
	Name     string `db:"name" json:"name"`
	SHA256   string `db:"sha256" json:"sha256"`
	FileType string `db:"f_type" json:"file_type"`
	Blob     []byte `db:"blob" json:"-"`
}

// // EncodeBase64 -
// func (file *File) EncodeBase64() {
// 	file.Blob = base64.StdEncoding.EncodeToString(file.BlobRaw)
// }

// // DecodeBase64 -
// func (file *File) DecodeBase64() error {
// 	data, err := base64.StdEncoding.DecodeString(file.Blob)
// 	if err != nil {
// 		return nil
// 	}
// 	file.BlobRaw = data
// 	return nil
// }

// User -
type User struct {
	ID     uint64 `db:"id" json:"id"`
	Name   string `db:"name" json:"name"`
	Digest string `db:"digest" json:"digest"`
}

type Token struct {
	Name   string `json:"name"`
	UserID uint64 `json:"user_id"`
	Admin  bool   `json:"admin"`
}

func (tk *Token) Valid() error {
	return nil
}

// Containers - alias
type Containers []Container

//Items - list Items alias
type Items []Item

// Users - alias
type Users []User

type Tags []string

type Files []File

// // Value -
// func (s Tags) Value() (driver.Value, error) {
// 	if len(s) == 0 {
// 		return "[]", nil
// 	}
// 	return fmt.Sprintf(`["%s"]`, strings.Join(s, `","`)), nil
// }

// // Scan -
// func (s *Tags) Scan(src interface{}) (err error) {
// 	var tags []string
// 	log.Print(src)
// 	switch src.(type) {
// 	case string:
// 		err = json.Unmarshal([]byte(src.(string)), &tags)
// 	case []byte:
// 		err = json.Unmarshal(src.([]byte), &tags)
// 	default:
// 		return errors.New("Incompatible type for Tags")
// 	}
// 	if err != nil {
// 		return
// 	}
// 	*s = tags
// 	return nil
// }
